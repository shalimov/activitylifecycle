package ru.pvolan.activitylifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ActivityA extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("ActivityLifecycle", "ActivityA onCreate");
        super.onCreate(savedInstanceState);
        Log.i("ActivityLifecycle", "ActivityA onCreate base finished");

        TextView tv = new TextView(this);
        tv.setText("ActivityA");
        tv.setBackgroundColor(0xffff0000);
        setContentView(tv);


        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityB.startActivity(ActivityA.this);
            }
        });


        Log.i("ActivityLifecycle", "ActivityA onCreate finished");
    }


    @Override
    protected void onStart() {
        Log.i("ActivityLifecycle", "ActivityA onStart");
        super.onStart();
        Log.i("ActivityLifecycle", "ActivityA onStart finished");
    }

    @Override
    protected void onResume() {
        Log.i("ActivityLifecycle", "ActivityA onResume");
        super.onResume();
        Log.i("ActivityLifecycle", "ActivityA onResume finished");
    }


    @Override
    protected void onPause() {
        Log.i("ActivityLifecycle", "ActivityA onPause");
        super.onPause();
        Log.i("ActivityLifecycle", "ActivityA onPause finished");
    }


    @Override
    protected void onStop() {
        Log.i("ActivityLifecycle", "ActivityA onStop");
        super.onStop();
        Log.i("ActivityLifecycle", "ActivityA onStop finished");
    }


    @Override
    protected void onDestroy() {
        Log.i("ActivityLifecycle", "ActivityA onDestroy");
        super.onDestroy();
        Log.i("ActivityLifecycle", "ActivityA onDestroy finished");
    }
}
