package ru.pvolan.activitylifecycle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ActivityB extends Activity {



    public static void startActivity(Context src){
        Intent i = new Intent(src, ActivityB.class);
        src.startActivity(i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("ActivityLifecycle", "ActivityB onCreate");
        super.onCreate(savedInstanceState);

        TextView tv = new TextView(this);
        tv.setText("ActivityB");
        tv.setBackgroundColor(0xff0000ff);
        setContentView(tv);


        Log.i("ActivityLifecycle", "ActivityB onCreate finished");
    }


    @Override
    protected void onStart() {
        Log.i("ActivityLifecycle", "ActivityB onStart");
        super.onStart();
        Log.i("ActivityLifecycle", "ActivityB onStart finished");
    }

    @Override
    protected void onResume() {
        Log.i("ActivityLifecycle", "ActivityB onResume");
        super.onResume();
        Log.i("ActivityLifecycle", "ActivityB onResume finished");
    }


    @Override
    protected void onPause() {
        Log.i("ActivityLifecycle", "ActivityB onPause");
        super.onPause();
        Log.i("ActivityLifecycle", "ActivityB onPause finished");
    }


    @Override
    protected void onStop() {
        Log.i("ActivityLifecycle", "ActivityB onStop");
        super.onStop();
        Log.i("ActivityLifecycle", "ActivityB onStop finished");
    }


    @Override
    protected void onDestroy() {
        Log.i("ActivityLifecycle", "ActivityB onDestroy");
        super.onDestroy();
        Log.i("ActivityLifecycle", "ActivityB onDestroy finished");
    }
}
